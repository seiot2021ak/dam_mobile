package com.example.dammobile;

import android.graphics.Color;

public enum Alert {
    NA(Color.parseColor("#646464"), false),
    NORMAL(Color.GREEN, false),
    PRE_ALARM(Color.YELLOW, false),
    ALARM(Color.RED, true);

    private final int color;
    private final boolean manualLock;

    Alert(int color, boolean manualLock) {
        this.color = color;
        this.manualLock = manualLock;
    }

    public int getColor() {
        return color;
    }

    public boolean canManualOverride() {
        return manualLock;
    }
}
