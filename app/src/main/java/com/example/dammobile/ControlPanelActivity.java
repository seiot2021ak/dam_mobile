package com.example.dammobile;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import me.aflak.bluetooth.Bluetooth;
import me.aflak.bluetooth.interfaces.DeviceCallback;

public class ControlPanelActivity extends AppCompatActivity {

    Bluetooth bluetooth;
    LiveBluetooth liveBluetooth = new LiveBluetooth(this);

    TextView alertLevelLabel;
    TextView waterLevelLabel;
    TextView damOpeningLabel;

    Button manualControlButton;
    SeekBar manualControl;

    private final Data data = new Data();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_panel);

        bluetooth = liveBluetooth.getBluetoothMutableLiveData().getValue();

        if(bluetooth == null){
            bluetooth = new Bluetooth(this);
            liveBluetooth.setBluetoothMutableLiveData(bluetooth);
        }

        bluetooth.setDeviceCallback(new DeviceCallback() {
            @Override
            public void onDeviceConnected(BluetoothDevice device) {
                Log.d("CONNECTION","Device connected");
                //bluetooth.send("DEBUG:ALL");
                bluetooth.send("INFO:STATUS");
            }
            @Override
            public void onDeviceDisconnected(BluetoothDevice device, String message) {}
            @Override
            public void onMessage(byte[] message) {
                String decodedMessage = new String(message);
                Log.d("MESSAGE", decodedMessage);

                String[] block = decodedMessage.split(":");
                if(block.length == 2) {
                    decodeMSG(block[0], block[1]);
                }

            }
            @Override
            public void onError(int errorCode) {
                Log.e("BT_DEVICE_CALLBACK", "Error on bluetooth: " + errorCode);
            }
            @Override
            public void onConnectError(BluetoothDevice device, String message) {
                Log.e("BT_DEVICE_CALLBACK", "Error on connect: " + message);
                Toast.makeText(ControlPanelActivity.this, "Connection Failed", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        initializeGUI();
        initializeDamControl();
    }

    @Override
    protected void onStart() {
        super.onStart();
        bluetooth.onStart();
        String tag = getIntent().getExtras().getString("tag");
        String value = getIntent().getExtras().getString(tag);
        if (bluetooth.isConnected()) {
            bluetooth.disconnect();
        }
        if(tag.equals("name")) {
            bluetooth.connectToName(value);
        } else if (tag.equals("address")){
            bluetooth.connectToAddress(value);
        }
        if (!bluetooth.isConnected()) {
            Toast.makeText(this, "Connection Failed", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(bluetooth.isConnected()) {
            bluetooth.disconnect();
        }
        bluetooth.onStop();
    }

    private void decodeMSG(String header, String content) {
        switch (header) {
            case "INFO":        // info, unused
                break;
            case "DEBUG":       // log.debug
                // use this section for DEBUG messages
                break;
            case "ALERT":       // change alert status
                switch (content) {
                    case "NORMAL":
                        setAlertStatus(Alert.NORMAL);
                        break;
                    case "PRE-ALARM":
                        setAlertStatus(Alert.PRE_ALARM);
                        break;
                    case "ALARM":
                        setAlertStatus(Alert.ALARM);
                        break;
                    default:
                        Log.e("MESSAGE_ALERT", content + " not recognized");
                }
                break;
            case "MANUAL":      // manual override
                /*  WARNING: EMERGENCY ONLY
                in case of testing or emergency
                the override can be activated remotely
                */
                manualControl.setEnabled(data.getAlertStatus().equals(Alert.ALARM));
                break;
            case "PERCENTAGE":  // dam water-valve opening %
                data.setValveOpening(Integer.parseInt(content));
                changeLabelText("PERCENTAGE", content);
                break;
            case "WATER":       // water level
                data.setWaterLevel(Integer.parseInt(content));
                changeLabelText("WATER", content);
                break;
            default:
                Log.e("DECODE_MESSAGE", "Header not recognized: " + header);
        }
    }

    private void setAlertStatus(Alert alertStatus) {
        data.setAlertStatus(alertStatus);
        alertLevelLabel.setText(alertStatus.toString());
        alertLevelLabel.setBackgroundColor(alertStatus.getColor());
        runOnUiThread(()->{
            manualControlButton.setEnabled(alertStatus.canManualOverride());
            manualControl.setEnabled(false);
        });

    }

    private void setWaterValveOpening(int progress) {
        if (0 <= progress && progress <= 10) {
            progress = 0;
        } else if (10 <= progress && progress <= 30) {
            progress = 20;
        } else if (30 <= progress && progress <= 50) {
            progress = 40;
        } else if (50 <= progress && progress <= 70) {
            progress = 60;
        } else if (70 <= progress && progress <= 90) {
            progress = 80;
        } else if (90 <= progress && progress <= 100) {
            progress = 100;
        }
        Log.d("DAM_OPENING", "Water-Valve opening set to: " + progress + "%");
        changeLabelText("PERCENTAGE", Integer.toString(progress));
    }

    private void changeLabelText(final String label, String content) {
        if(data.getAlertStatus()!=null){
            switch (label) {
                case "PERCENTAGE":
                    if (data.getAlertStatus().equals(Alert.ALARM)) {
                        content = content.concat("%");
                    } else {
                        content = "----";
                    }
                    waterLevelLabel.setText(content);
                    break;
                case "WATER":
                    if (data.getAlertStatus().equals(Alert.PRE_ALARM) || data.getAlertStatus().equals(Alert.ALARM)) {
                        content = content.concat(" meters");
                    } else {
                        content = "----";
                    }
                    damOpeningLabel.setText(content);
                    break;
            }
        }

    }

    private void initializeGUI() {
        alertLevelLabel = findViewById(R.id.alert_level);
        waterLevelLabel = findViewById(R.id.water_level);
        damOpeningLabel = findViewById(R.id.dam_opening);
    }

    private void initializeDamControl() {
        manualControlButton = findViewById(R.id.manual_button);
        manualControl = findViewById(R.id.dam_opening_control);
        manualControl.setMax(100);

        manualControlButton.setOnClickListener(v -> {
            manualControl.setEnabled(true);
            bluetooth.send("MANUAL:TRUE");
        });
        manualControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    this.progress = progress;
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                setWaterValveOpening(progress);
            }
        });
    }

}

class LiveBluetooth{

    private final MutableLiveData<Bluetooth> bluetoothMutableLiveData = new MutableLiveData<>();

    LiveBluetooth(Context context){
        bluetoothMutableLiveData.setValue(new Bluetooth(context));
    }

    public MutableLiveData<Bluetooth> getBluetoothMutableLiveData() {
        return bluetoothMutableLiveData;
    }

    public void setBluetoothMutableLiveData(Bluetooth bluetooth) {
        this.bluetoothMutableLiveData.setValue(bluetooth);
    }
}
