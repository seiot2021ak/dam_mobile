package com.example.dammobile;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import pub.devrel.easypermissions.EasyPermissions;

@RequiresApi(api = Build.VERSION_CODES.S)
public class MainActivity extends AppCompatActivity {

    //permissions
    String[] permissions = {
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.BLUETOOTH_SCAN,
            Manifest.permission.BLUETOOTH_CONNECT,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION
    };
    Integer RC_BLUETOOTH = 123;
    boolean permissionsGranted = false;

    //debug_mode
    static final String DEFAULT_ADDRESS = "20:16:05:24:66:76";
    private Button DEFAULT_CONNECT;

    //Connection by name
    private EditText deviceNameET;
    private Button connectName;
    //Connection by address
    private EditText deviceAddressET;
    private Button connectAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermissions();
        initializeUI();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void requestPermissions() {
        if (EasyPermissions.hasPermissions(this, permissions)) {
            // Already have permission, do the thing
            permissionsGranted = true;
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this,
                    "Bluetooth access is needed to use this app", RC_BLUETOOTH, permissions);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private void initializeUI() {
        deviceNameET = findViewById(R.id.device_name_edit);
        connectName = findViewById(R.id.device_name_bt);
        deviceAddressET = findViewById(R.id.device_address_edit);
        connectAddress = findViewById(R.id.device_address_bt);
        DEFAULT_CONNECT = findViewById(R.id.default_connect);

        connectName.setOnClickListener(v -> {
            String deviceName = deviceNameET.getText().toString();
            Log.d("CONNECT", "Device Name: " + deviceName);
            connectActivity(deviceName, "name");
        });

        connectAddress.setOnClickListener(v -> {
            String deviceAddress = deviceAddressET.getText().toString();
            Log.d("CONNECT", "Device Address: " + deviceAddress);
            connectActivity(deviceAddress, "address");
        });

        DEFAULT_CONNECT.setOnClickListener(v -> {
            Log.d("CONNECT", "Device Address: " + DEFAULT_ADDRESS);
            connectActivity(DEFAULT_ADDRESS, "address");
        });
    }

    void connectActivity(String value, String tag) {
        Intent intent = new Intent(this, ControlPanelActivity.class);
        intent.putExtra("tag", tag);
        intent.putExtra(tag, value);
        startActivity(intent);
    }
}