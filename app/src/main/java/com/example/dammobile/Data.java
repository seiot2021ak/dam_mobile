package com.example.dammobile;

public class Data {

    private Alert alertStatus = Alert.NA;
    private int waterLevel = 0;
    private int valveOpening = 0;

    public Data() {

    }

    public Alert getAlertStatus() {
        return alertStatus;
    }

    public void setAlertStatus(Alert alertStatus) {
        this.alertStatus = alertStatus;
    }

    public int getWaterLevel() {
        return waterLevel;
    }

    public void setWaterLevel(int waterLevel) {
        this.waterLevel = waterLevel;
    }

    public int getValveOpening() {
        return valveOpening;
    }

    public void setValveOpening(int valveOpening) {
        this.valveOpening = valveOpening;
    }

}
